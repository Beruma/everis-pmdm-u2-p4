package dam2.pmdm.u2.pmdm_p4_beatriz_ruiz_maximo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    WebView simpleWebView;
    EditText url;
    TextView respuesta;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        simpleWebView = (WebView) findViewById(R.id.simpleWebView);
        url = (EditText) findViewById(R.id.url);
        respuesta = (TextView) findViewById(R.id.respuesta);
        boton = (Button) findViewById(R.id.boton);

    }

    public void entrarPagina(View vista){
        String direccion = url.getText().toString();
        if (direccion.length() > 0){
            String comprobar = url.getText().toString().substring(0, 7);
            if (comprobar.equals("http://")) {
                simpleWebView.loadUrl(url.getText().toString());
            }else{
                respuesta.setText("Por favor, introduce https://");
            }
        }else{
            respuesta.setText("Debes introdicr una url");
        }
    }
}